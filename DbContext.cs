using System;
using Microsoft.EntityFrameworkCore;
using Rubius.Model;

namespace Rubius.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {            
        }

        public DbSet<ProjectEntry> ProjectEntry { get; set; }
    }
}