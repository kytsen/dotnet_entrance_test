﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Rubius.Model;
using Rubius.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;

namespace Rubius.Pages
{
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _db;
        
        public List<String> projectsList = new List<String>(new string[] { "Planyway", "Rubius Project Manager"});
        
        [BindProperty]
        public ProjectEntry projectEntry { get; set; }
        
        [BindProperty]
        public IList<ProjectEntry> projectEntries { get; set; }

        public PaginatedList<ProjectEntry> projectEntryPaginated { get; set; }

        public IndexModel(AppDbContext db)
        {
            _db = db;
        }
        
        public async Task OnGetAsync(string sortOrder, string currentFilter,int? pageIndex)
        {
            /**
            CurrentSort = sortOrder;

            pageIndex = 1;

            IQueryable<ProjectEntry> projectEntryIq = from s in _db.ProjectEntry.ToList().AsQueryable()
                                            select s;
            switch (sortOrder)
            {
                case "project":
                    projectEntryIq = projectEntryIq.OrderByDescending(s => s.Project);
                    break;
                default:
                    projectEntryIq = projectEntryIq.OrderBy(s => s.Date);
                    break;
            }
                int pageSize = 5;
                projectEntryPaginated = await PaginatedList<ProjectEntry>.CreateAsync(
                projectEntryIq.AsNoTracking(), pageIndex ?? 1, pageSize);
            
                
            **/
            projectEntries = await _db.ProjectEntry.AsNoTracking().ToListAsync();
        }

        // public string CurrentSort { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return RedirectToPage();
            }
            if (projectsList.Contains(projectEntry.Project))
            {
                projectEntry.Date = Convert.ToDateTime(Regex.Replace(projectEntry.Date.ToString(), @"[^a-zA-Z0-9\ /:]", ""));
                projectEntry.Project = Regex.Replace(projectEntry.Project, @"[^a-zA-Z0-9\ /]", "");
                projectEntry.Comment = Regex.Replace(projectEntry.Comment, @"[^a-zA-Z0-9\ /]", "");
                _db.ProjectEntry.Add(projectEntry);
                await _db.SaveChangesAsync();
            }
            return RedirectToPage();
        }

        public async Task<IActionResult>OnPostDeleteAsync(int id)
        {
            var projectEntry = await _db.ProjectEntry.FindAsync(id);

            if (projectEntry != null)
            {
                _db.ProjectEntry.Remove(projectEntry);
                await _db.SaveChangesAsync();
            }
            return RedirectToPage();
        }
    }

}