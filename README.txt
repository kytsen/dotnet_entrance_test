This is entrance test I have to do for a company specialized in C# development.

Features implemented
- ASP.NET Core 2.0 
- materialize 0.100.2 is used
- Form with 
    1. Report date;
    2. Project (drop-down list);
    3. Comments
- check required input    
- filter input value
    
Paging/sorting/filtering was not implemented.
I follow this tutorial to build the application https://docs.microsoft.com/en-us/aspnet/core/mvc/razor-pages/?tabs=visual-studio
Even if I find the correct sources to develop this features, It seems to required to rebuild a part of the application https://docs.microsoft.com/en-us/aspnet/mvc/overview/getting-started/getting-started-with-ef-using-mvc/sorting-filtering-and-paging-with-the-entity-framework-in-an-asp-net-mvc-application#add-paging-to-the-students-index-page

Features possible to improve the app:i
- paging, sorting and filtering
- replace post method by full asynchronous javascript request

A lot of things could have been asked about the user needs, for example:
- date format
- appareance of the user interface
- animation

To run the project
dotnet build && dotnet run
