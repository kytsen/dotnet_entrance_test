using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Rubius.Data;
using Rubius.Model;

namespace Rubius
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options =>
                              options.UseInMemoryDatabase("name"));       
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, AppDbContext db)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });
           
        
            ProjectEntry projectEntry = new ProjectEntry();
            projectEntry.Date = DateTime.Parse("12/9/2017");
            projectEntry.Project = "Planyway";
            projectEntry.Comment = "A very effective application to manage hours and pay.";
            db.ProjectEntry.Add(projectEntry);
            projectEntry = new ProjectEntry();
            projectEntry.Date = DateTime.Parse("12/10/2017");
            projectEntry.Project = "Planyway";
            projectEntry.Comment = "Very dynamic support which help me to fix problem quickly.";
            db.ProjectEntry.Add(projectEntry);
            projectEntry = new ProjectEntry();
            projectEntry.Date = DateTime.Parse("12/13/2017");
            projectEntry.Project = "Rubius Project Manager";
            projectEntry.Comment = "Thanks to last feature, I was able to finish my project in time. Great job!";
            db.ProjectEntry.Add(projectEntry);
            db.SaveChangesAsync();
        }
    }
}


