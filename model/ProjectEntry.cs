using System;
using System.ComponentModel.DataAnnotations;
namespace Rubius.Model
{
    public class ProjectEntry
    {
        public int Id { get; set; }

        [Required]
        public DateTime Date { get; set; }


        [Required, StringLength(300)]
        public string Project { get; set; }


        [Required, StringLength(300)]
        public string Comment { get; set; }
    }
}